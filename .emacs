;; Configuração básica do emacs

(setq inhibit-startup-message t)              ; Remoção do buffer de boas-vindas

(tool-bar-mode -1)                            ; Remoção da barra de ferramentas
(menu-bar-mode -1)                            ; Remoção da barra de menu
(scroll-bar-mode -1)                          ; Remoção da barra de rolagem

(global-display-line-numbers-mode t)          ; Ativa a enumeração de linhas
(column-number-mode t)                        ; Exibe coluna atual na modeline
(global-hl-line-mode t)                       ; Exibe destaque de linha

;; Espaçamento das bordas laterais
(set-fringe-mode 10)

(global-unset-key (kbd "C-z"))                ; Desabilita  Ctrl-Z (suspend frame)
(delete-selection-mode t)                     ; O texto digitado substitui a seleção

;; Rolagem mais suave
(setq mouse-wheel-scroll-amount '(2 ((shift) .1))    ;  2 linhas por vez
      mouse-wheel-progressive-speed nil              ;  Não acelera a rolagem
      mouse-wheel-follow-mouse 't                    ;  Rola a janela sob o mouse
      scroll-step 1)                                 ;  Rola 1 linha com teclado

;; Quebra de linha
(global-visual-line-mode t)

;; Backups
(setq backup-directory-alist `(("." . "~/.saves")))'


;; Tipo de cursor
(setq-default cursor-type 'bar)


;; Verifica e inicia o package.el
(require 'package)

;; Definição de repositórios
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

;; Inicialização do sistema de pacotes
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Instalação do use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (dichromacy)))
 '(package-selected-packages
   (quote
    (color-theme tango-dark tango-dark-theme company haskell-mode lsp-haskell lsp-ivy dap-mode helm-lsp lsp-treemacs flycheck lsp-ui lsp-mode hindent proof-general use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; if you want to change prefix for lsp-mode keybindings.
(setq lsp-keymap-prefix "s-l")

(require 'lsp-mode)
(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)
