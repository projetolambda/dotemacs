(define-package "helm-core" "20220217.812" "Development files for Helm"
  '((emacs "25.1")
    (async "1.9.4"))
  :commit "f030e584b7f2c8679297ce960cedbdf0f7e7e86e" :authors
  '(("Thierry Volpiatto" . "thierry.volpiatto@gmail.com"))
  :maintainer
  '("Thierry Volpiatto" . "thierry.volpiatto@gmail.com")
  :url "https://emacs-helm.github.io/helm/")
;; Local Variables:
;; no-byte-compile: t
;; End:
