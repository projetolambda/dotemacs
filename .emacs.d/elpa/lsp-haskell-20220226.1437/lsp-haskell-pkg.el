;;; -*- no-byte-compile: t -*-
(define-package "lsp-haskell" "20220226.1437" "Haskell support for lsp-mode" '((emacs "24.3") (lsp-mode "3.0")) :commit "69ddd5d32d6d7d658ec3f89c8ec6280e912e6be8" :keywords '("haskell") :url "https://github.com/emacs-lsp/lsp-haskell")
