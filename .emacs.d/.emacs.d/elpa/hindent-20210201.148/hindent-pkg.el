;;; -*- no-byte-compile: t -*-
(define-package "hindent" "20210201.148" "Indent haskell code using the \"hindent\" program" '((cl-lib "0.5")) :commit "f7e7b12fd119e91c795cbe21bebf7b5af5d273b8" :authors '(("Chris Done" . "chrisdone@gmail.com")) :maintainer '("Chris Done" . "chrisdone@gmail.com") :url "https://github.com/chrisdone/hindent")
